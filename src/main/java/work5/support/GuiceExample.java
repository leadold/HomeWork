package work5.support;

import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.WebDriver;

@ScenarioScoped
public class GuiceExample {
    public WebDriver driver = new WebDriverFactory().getDriver();
}
