package work5.support;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Locale;

public class WebDriverFactory {
    public WebDriver getDriver(){
        String browser = System.getProperty("browser").trim().toLowerCase();

        switch (browser){

            case "chrome":
                WebDriverManager.chromedriver().setup();
                return  new ChromeDriver();

            case "Firefox":
                WebDriverManager.chromedriver().setup();
                return  new FirefoxDriver();
        }
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }
}
