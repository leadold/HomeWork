package work5.ru.pochta.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import work5.support.GuiceExample;

public class PochtaResultPage extends BasePage<PochtaResultPage> {
// тут последний шаг будет для проверки
    @FindBy(xpath = "//*[@id=\"page-tracking\"]/div/div[2]/div[2]/div[1]/div/div[1]/div/p")
    private WebElement firstText;

    @Inject
    public PochtaResultPage(GuiceExample guiceExample){
        super(guiceExample, "");
    }

    public String getResulText(){
        return firstText.getText();
    }
}
