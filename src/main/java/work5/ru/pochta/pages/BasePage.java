package work5.ru.pochta.pages;

import org.openqa.selenium.support.PageFactory;
import work5.support.GuiceExample;

public abstract class BasePage<T> {
    protected GuiceExample guiceExample;
    protected String path;

    public BasePage(GuiceExample guiceExample, String path){
        PageFactory.initElements(guiceExample.driver, this);
        this.guiceExample = guiceExample;
        this.path = path;
    }

    public T open(){
        guiceExample.driver.get((System.getProperty("base.url" ) + path));
        return(T) this;
    }
}
