package work5.ru.pochta.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import work5.support.GuiceExample;

public class PochtaMainPage extends  BasePage<PochtaMainPage>{

    @FindBy(xpath = "//*[@id=\"__next\"]/div/div[1]/div[1]/div/div/div/div[4]/div")
    private WebElement menu1;
    @FindBy(xpath = "//*[@id=\"__next\"]/div/div[1]/div[1]/div/div/div/div[5]/div[1]/div[1]/div[1]/a")
    private WebElement button1;

    @FindBy(id = "username")
    private WebElement fieldLogin;
    @FindBy(id = "userpassword")
    private WebElement fieldPassword;
    @FindBy(xpath = "//*[@id=\"loginForm\"]/button")
    private WebElement buttonLogin;

    @FindBy(xpath = "//*[@id=\"__next\"]/div/div[1]/div[1]/div/div/div/div[4]/div[2]")
    private WebElement menu2;
    @FindBy(xpath = "//*[@id=\"__next\"]/div/div[1]/div[1]/div/div/div/div[5]/div[1]/div[1]/div[1]/div[2]/a")
    private WebElement button2;


    @Inject
    public PochtaMainPage(GuiceExample guiceExample){
        super(guiceExample, "");
    }

    public void clickMenu(){
        menu1.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickButton1(){
        button1.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public PochtaMainPage enterLogin(String text){
        fieldLogin.sendKeys(text);
        return this;
    }

    public PochtaMainPage enterPassword(String text){
        fieldPassword.sendKeys(text);
        return this;
    }

    public void clickButtonLogin(){
        buttonLogin.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickMenu2(){
        menu2.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickButton2(){
        button2.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
