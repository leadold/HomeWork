package Work3;

import Work3.Man.*;

public class main {
    public static void main(String[] args) {
        String year = null;

        Man[] mans = new Man[3];
        mans[0] = new Ivan("Ivan", "Ivanov", 18);
        mans[1] = new Ivan("Ivan", "Ivanov", 2);
        mans[2] = new Ivan("Ivan", "Ivanov", 1);

        for(Man a : mans){
            if (a.getAge() == 1) {
                year = "год";
            }else if (a.getAge() >= 5){
                year = "лет";
            }else {
                year = "года";
            }

            System.out.println(a.toString() + " " + year);


            Loginable user = new User("Ivan", "Ivanov", 1);
            user.login();
        }
    }
}
