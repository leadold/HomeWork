package Work3.Man;

import Work3.Man.*;

public class Ivan extends Man {


    public Ivan(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void Say() {
        System.out.println("Я говорю!");
    }

    @Override
    public void Go() {
        System.out.println("Я иду!");
    }

    @Override
    public void Drink() {
        System.out.println("Я пью!");
    }

    @Override
    public void Eat() {
        System.out.println("Я ем!");
    }
}
