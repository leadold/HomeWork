package Work3.Man;

public abstract class Man {

    private String name;
    private String surname;
    private int age;

    public Man (String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getSurname() {return surname;}

    public void setSurname(String surname) {this.surname = surname;}

    public void setAge(int age) {this.age = age;}

    public int getAge() {return age;}

    public abstract void Say();

    public abstract void Go();

    public abstract void Drink();

    public abstract void Eat();

    @Override
    public String toString() {
        return "Привет! меня зовут " +
                name + ' ' +
                surname + ' ' +
                ", мне " + age;
    }
}
