package Work3.Man;

public class User extends Man implements Loginable{

    public User(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void Say() {
        System.out.println("Я работаю в company");
    }

    @Override
    public void Go() {

    }

    @Override
    public void Drink() {

    }

    @Override
    public void Eat() {

    }

    @Override
    public void login() {
        System.out.println("Авторизация с логином login с паролем password");
    }

    @Override
    public void password() {

    }

    @Override
    public void company() {

    }
}
