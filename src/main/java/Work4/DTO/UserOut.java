package Work4.DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@EqualsAndHashCode
@JsonSerialize
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserOut {

    private long code;
    private String message;
    private String type;

}

