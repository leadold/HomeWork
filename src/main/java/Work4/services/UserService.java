package Work4.services;

//import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.response.Response;
import io.restassured.http.ContentType;
import Work4.DTO.User;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class UserService {
//    OpenApiValidationFilter filter = new OpenApiValidationFilter("https://petstore.swagger.io/v2/swagger.json");
    private RequestSpecification spec;

    public UserService(){
        spec = given()
                .baseUri("https://petstore.swagger.io/v2")
//                .filter(filter)
                .log().all()
                .contentType(ContentType.JSON);

    }

    public Response CreateUser (User user){
        return given(spec)
                .body(user)
                .when()
                .post("/user");
    }

    public Response GetUserByName(String name){
        return given(spec)
                .when()
                .get("/user/" + name);
    }

    public Response CreateUserWithArray (User user){
        return given(spec)
                .body(user)
                .when()
                .post("/user/createWithArray");
    }
}
