package Work4.generators;

import Work4.DTO.User;

public class UserGenerator {
    private static long COUNT = 0;
    public static User getNewUser(){
        COUNT++;
        return User.builder()
                .id(COUNT)
                .email("EMAIL")
                .firstName("First name")
                .lastName("Last name")
                .password("PASSWORD" + COUNT)
                .phone("8(999)999-99-99")
                .username("USERNAME" + COUNT)
                .userStatus(2)
                .build();
    }

        public static User getExisUser () {
        COUNT++;
        return User.builder()
                .id(COUNT)
                .email("EMAIL")
                .firstName("First name")
                .lastName("Last name")
                .password("PASSWORD" + COUNT)
                .phone("8(999)999-99-99")
                .username("USERNAME" + COUNT)
                .userStatus(2)
                .build();
    }

}
