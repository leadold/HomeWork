package Work1;

public class main {

    public static void main(String[] args) {

        int a = 100;
        int[] array = new int[a];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        int max = array[0];
        int min = array[0];
        int avgarray = array[0];
        int avg;

        for (int i = 0; i < array.length; i++) {
            if(max < array[i])
                max = array[i];
            if(min > array[i])
                min = array[i];
            avgarray += array[i];
        }

        avg = avgarray / array.length;

        System.out.println("max = " + max);
        System.out.println("min = " + min);
        System.out.println("avg = " + avg);
    }
}
