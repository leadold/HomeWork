# language: ru
@search
Функционал: Авторизация
@success @smoke @regress
  Структура сценария: : Проверка авторизации
    Пусть Открыта главная страница в браузере
    Если Нажать на меню до авторизации
    Если Нажать кнопку войти в меню
    Если Ввести в логин "<Логин>"
    Если Ввести в пароль "<Пароль>"
    Если Нажать кнопку войти
    Если Нажать на меню после авторизации
    Если Нажать кнопку войти в мои отправления
    Тогда Текст для начала работы "<Результат>"


    Примеры:
    |Логин|Пароль|Результат|
    |tojoxi2718@mahazai.com|1QAZ2wsx|Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся|