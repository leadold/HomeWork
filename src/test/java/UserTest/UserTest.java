package UserTest;

import Work4.DTO.User;
import Work4.DTO.UserOut;
import Work4.generators.UserGenerator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import Work4.services.UserService;

public class UserTest {
    @Test
    public void createUserTest(){

        User user = UserGenerator.getNewUser();

        UserService userService = new UserService();
        Response response = userService.CreateUser(user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message(user.getId().toString())
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @ValueSource(strings = {"admin", "moderator", "user"})
    public void getUserByName(String name){
        UserService userService = new UserService();
        Response response = userService.GetUserByName(name);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(404)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(1)
                .message("User not found")
                .type("error")
                .build();

        Assertions.assertEquals(expected, actual);

    }
}