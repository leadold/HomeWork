package UserTest;

import Work4.DTO.User;
import Work4.DTO.UserOut;
import Work4.generators.UserGenerator;
import Work4.services.UserService;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserArrayTest {
    @Test
    public void createUserTest() {

        User user = UserGenerator.getNewUser();

        UserService userService = new UserService();
        Response response = userService.CreateUserWithArray(user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message(user.getId().toString())
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);

    }
}
