package work5.ru.pochta.hooks;

import com.google.inject.Inject;
import io.cucumber.java.After;
import work5.support.GuiceExample;

public class Hooks {

    @Inject
    private GuiceExample guiceExample;

    @After(order = 1)
    public void CloseDriver(){
        if(guiceExample.driver != null)
            guiceExample.driver.quit();
    }
}
