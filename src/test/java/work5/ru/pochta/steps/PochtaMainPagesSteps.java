package work5.ru.pochta.steps;

import io.cucumber.java.ru.Если;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import work5.ru.pochta.pages.PochtaMainPage;
import com.google.inject.Inject;

public class PochtaMainPagesSteps {

    @Inject
    private PochtaMainPage pochtaMainPage;

    @Пусть("Открыта главная страница в браузере")
    public void openMainPage(){
        pochtaMainPage.open();
    }

    @Если ("Нажать на меню до авторизации")
    public void clickMenu(){
        pochtaMainPage.clickMenu();
    }

    @Если ("Нажать кнопку войти в меню")
    public void clickButton1(){
        pochtaMainPage.clickButton1();
    }

    @Если("Ввести в логин {string}")
    public void enterLogin(String text){
        pochtaMainPage.enterLogin(text);
    }

    @Если("Ввести в пароль {string}")
    public void enterPassword(String text){
        pochtaMainPage.enterPassword(text);
    }

    @Если ("Нажать кнопку войти")
    public void clickButtonLogin(){
        pochtaMainPage.clickButtonLogin();
    }

    @Если ("Нажать на меню после авторизации")
    public void clickMenu2(){
        pochtaMainPage.clickMenu2();
    }

    @Если ("Нажать кнопку войти в мои отправления")
    public void clickButton2(){
        pochtaMainPage.clickButton2();
    }
}
