package work5.ru.pochta.steps;

import com.google.inject.Inject;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import work5.ru.pochta.pages.PochtaResultPage;

public class PochtaResultPageSteps {

    @Inject
    PochtaResultPage pochtaResultPage;

    @Тогда("Текст для начала работы {string}")
    public void assertText(String expectedText){
        Assertions.assertEquals(expectedText, pochtaResultPage.getResulText());
    }
}
